<?php

namespace model;
include 'Db.php';

class Preview
{

    public $id;
    public $experiment;
    public $variant;
    public $loadedAt;

    /**
     * Preview constructor.
     * @param null $id
     * @param $experiment
     * @param $variant
     * @param null $loadedAt
     */
    public function __construct($id = null, $experiment, $variant, $loadedAt = null)
    {
        $this->id = $id;
        $this->experiment = $experiment;
        $this->variant = $variant;
        $this->loadedAt = $loadedAt;
    }

    /**
     * @return bool
     */
    public function saveNew()
    {
        $db = Db::getInstance();
        // we make sure $id is an integer
        $req = $db->prepare('INSERT INTO preview (experiment , variant) VALUES (:experiment , :variant);');
        $req->bindValue(':experiment', $this->experiment, \PDO::PARAM_STR);
        $req->bindValue(':variant', $this->variant, \PDO::PARAM_STR);
        return $req->execute();
    }

    /**
     * @return Preview
     */
    public static function findMax()
    {
        $db = Db::getInstance();
        // we make sure $id is an integer
        $req = $db->query('SELECT * FROM preview ORDER BY id DESC LIMIT 1');
        // the query was prepared, now we replace :id with our actual $id value
        $post = $req->fetch();

        return new Preview($post['id'], $post['experiment'], $post['variant'], $post['loaded_at']);
    }
}