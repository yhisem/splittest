<?php

namespace model;


class Db
{
    private static $instance = NULL;

    /**
     * Db constructor.
     */
    private function __construct()
    {
    }

    /**
     *
     */
    private function __clone()
    {
    }

    /**
     * @return null|\PDO
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $pdo_options[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;
            self::$instance = new \PDO('mysql:host=localhost;dbname=split_test', 'root', '', $pdo_options);
        }
        return self::$instance;
    }
}

