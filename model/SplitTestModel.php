<?php

namespace model;
include 'Preview.php';

class SplitTestModel
{

    /**
     * @var array
     */
    protected $config = [
        'experiment1' => [
            'variant1' => 1,
            'variant2' => 1
        ],
        'experiment2' => [
            'variant1' => 3,
            'variant2' => 2
        ],
        'experiment3' => [
            'variant1' => 8,
            'variant2' => 1,
            'variant3' => 1
        ]
    ];

    protected $newPreview = 0;

    /**
     * @var int
     */
    private $userId = 1;


    /**
     * @param $splitName
     * @return int|string
     * @throws Exception
     */
    public function run($splitName)
    {
        if (!isset($this->config[$splitName])) {
            throw new Exception('Invalid split name', 500);
        } elseif (!is_string($splitName)) {
            throw new Exception('Invalid argument exception', 500);
        }
        $experiment = $this->config[$splitName];
        $splitKey = $this->getSplitKey(array_sum($experiment));
        $totalPercent = 0;
        foreach ($experiment as $key => $value) {
            $totalPercent += $value;
            if ($totalPercent > $splitKey) {
                if ($this->newPreview) {
                    SetCookie("previewId", $this->userId, time() + 2592000);
                    $model = new Preview(null, $splitName, $key, null);
                    $model->saveNew();
                }
                return $key;
            }
        }
    }


    /**
     * SplitTestModel constructor.
     */
    public function __construct()
    {
        if (isset($_COOKIE['previewId'])) {
            $this->userId = $_COOKIE['previewId'];
        } else {
            $prevUser = Preview::findMax();
            $this->newPreview = 1;
            if ($prevUser) {
                $this->userId = $prevUser->id + 1;
            }
        }
    }


    /**
     * @param $round
     * @return int
     */
    private function getSplitKey($round)
    {
        $result = $this->userId % 100;
        if (!is_null($round)) {
            $result = $result % $round;
        }

        return $result;
    }

}