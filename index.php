<?php

include 'model/SplitTestModel.php';

try {
    $res = new \model\SplitTestModel();

    /**
     * For change distribution , change $param in $res->run($param) , $param can be one of :
     *
     * 'experiment1' => "50% x 50%",
     * 'experiment2' => "60% x 40%",
     * 'experiment3' => "80% x 10% x10%"
     */
    echo $res->run('experiment1');
} catch (\Exception $e) {
//    log exception and return default
    echo $e;
}